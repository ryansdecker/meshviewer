//rsd - ryansdecker@gmail.com

#include <iostream>
//libigl - note Eigen and other libs/include dirs stated in Makefile
//you also need openGL for the viewer
#include <igl/readSTL.h> 
#include <igl/viewer/Viewer.h>
#include <igl/jet.h>
#include <GLFW/glfw3.h>
#include <glew.h>

//global debugging flag
bool DEBUG;
//input data
Eigen::MatrixXd V;//vertex
Eigen::MatrixXi F;//face
Eigen::MatrixXi N;//normal
Eigen::MatrixXd C;//colors

//keypress callback
bool key_down(igl::viewer::Viewer& viewer, unsigned char key, int modifier){
  if(DEBUG)
    std::cout<<"Key pressed: "<<key<<std::endl;
  if(key == ' '){//spacebar
    if(DEBUG)
      std::cout<<"Shading blue to red based on height"<<std::endl;
    //extract Z value (height)
    Eigen::VectorXd Z = V.col(2);
    igl::jet(Z,true,C);
    //Update display
    viewer.data.set_colors(C);
  }//spacebar pressed
  return true;
};

////MAIN//////////////////////////
int main(int argc, char* argv[]){
  //checks for argument
  if (argv[1]){
    //check for debug mode
    if(argv[2]){
      DEBUG = true;
    }
    //get filename
    std::string STLIn;
    STLIn = argv[1];
    if(DEBUG)
      std::cout<<"File = "<<STLIn<<std::endl;
    //read input file
    if(DEBUG)
      std::cout<<"reading stl"<<std::endl;
    igl::readSTL(STLIn, V, F, N); 
    if(DEBUG){//verify input file
      std::cout<<"size V = "<<V.size()<<std::endl;
      std::cout<<"size F = "<<F.size()<<std::endl;
      std::cout<<"size N = "<<N.size()<<std::endl;
    }
    //Display input file
    igl::viewer::Viewer viewer;
    viewer.data.set_mesh(V, F);
    viewer.callback_key_down = &key_down;
    viewer.launch();
  }
  else{//didn't pass an arguent
    std::cout<<"Usage: ./meshViewer [.stl to open] [DEBUG]"<<std::endl;
  }
  return 0;
}//end of main


