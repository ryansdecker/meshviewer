  # the compiler: gcc for C program, define as g++ for C++
	  CC = g++

	#you may need to change for your system
	INC = -I./GL/ -I./glew/include/GL -I./glfw/include -I./libigl/include/ -I/usr/include/eigen3/ -I/usr/local/include/ -I/usr/local/lib/ -I/usr/lib/x86_64-linux-gnu/ 
	LIBS = -lGLEW -lglfw3 -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -lm -pthread -ldl

	# compiler flags:
	  #  -g    adds debugging information to the executable file
		#  -Wall turns on most, but not all, compiler warnings
		#  -w turns off warnings
			  CXXFLAGS  = -g -Wall -w -std=c++11 $(INC) $(LIBS) 

  # the build target executable:
	  TARGET = meshViewer

  all: $(TARGET)
  $(TARGET): $(TARGET).cpp
			$(CC) -o $(TARGET) $(TARGET).cpp $(CXXFLAGS) 

  clean:
			$(RM) $(TARGET)
